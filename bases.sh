#!/bin/bash

base=10;
max=1000000;
limit=1000;
dir=batch;
time=/usr/bin/time;

[ $# -gt 0 ] && base=$1;
[ $# -gt 1 ] && max=$2;
[ $# -gt 2 ] && dir=$3;

[ -d ${dir} ] || mkdir ${dir};
pre="${dir}/base${base}";

# clearout of all the resulting files;
[ -f ${pre}.time		] && rm ${pre}.time;		# total time
[ -f ${pre}.step.time	] && rm ${pre}.step.time;	# time of each batch
[ -f ${pre}.txt			] && rm ${pre}.txt;			# lychrel output

# obase: 2 -> 16	gets a normal representation from bc
# obase > 17		gets a denary representation of the digits
if [ "${base}" -le 16 ]; then
	seq $base $max | header -a "obase=${base};" | bc | tr '\n' '\0';
else
	seq $base $max | header -a "obase=${base};" | bc | sed 's/^ //;s/ /,/g' | tr '\n' '\0';
fi |\
	${time} -o ${pre}.time xargs -0 -s 4000 ${time} -ao ${pre}.step.time ./lychrel -l ${limit} -b ${base} |\
	tee -a ${pre}.txt | nl -w6 -nrn;

# http://stackoverflow.com/a/14472352
