#include <string>
#include <iostream>
#include <sstream>
#include <fstream>

#include <vector>

#include <limits>
#include <getopt.h>

#include "methods.h"

// digit type
#ifdef DIGIT_TYPE
using digit = DIGIT_TYPE;
#else
using digit = uint8_t;
#endif

// Options - global because they may be queried globally
const struct option longopts[] = {
	{ "help",		no_argument,	0, 'h' },

	{ "verbose",	no_argument,	0, 'v' },
	{ "terse",		no_argument,	0, 't' },
	{ "csv",		no_argument,	0, 'c' },

	{ "base",	required_argument,	0, 'b' },
	{ "limit",	required_argument,	0, 'l' },
	{ "input",	required_argument,	0, 'i' },

	{ "first",		no_argument,	0, 'F' },
	{ "last",		no_argument,	0, 'L' },
	{ "version",	no_argument,	0, 'V' },

	{ 0, 0, 0, 0 }
};

/*	Possible flags
	--help		-h
	--verbose	-v
	--terse		-t
	--csv

	--base		-b
	--limit		-l
	--input		-i

	--first
	--last
	--version	-V

	--first
		stop after first argument that fails to terminate within the given limit
	--last
		print the last iteration reached (whether successful or not)
	--version (-V)
		extra start-up information
		e.g.	base: 10
				max:  65535
				digit_type: 16 bit unsigned int
	-vv       > 1
		print every iteration to stderr
		e.g.	./lychrel -vv 176
				176
				847
				1595
				7546
				14003
				176 in 5 iterations, became 44044
	--verbose > 0
		timing information
		e.g.	0:3
				1000:386
				2000:752
				3000:1139
				4000:1491
				19D after 5000 iterations, has FAILED at length 1863
	(normal) == 0
		full output
		e.g.	"62,63" in 10 iterations, became "31,56,33,16,33,56,31"
				19D after 1000 iterations, has FAILED at length 386
	--terse  <  0
		shorter csv style format
		e.g.	"62,63",10,"31,56,33,16,33,56,31"
				19D,1000,FAILED
	--csv    < -1
		output valid csv with header
		e.g.	number,iterations,result
				"62,63",10,"31,56,33,16,33,56,31"
				19D,1000,FAILED
*/

std::string usage( std::string name ){
	name += " { list of numbers... }\n"
		"  -b --base     numeric base (2-64) (default: 10)\n"
		"  -l --limit    iteration limit (default: 1000)\n"
		"  -i --input    input file to read from\n"
		"\n"
		"  -h --help     this help\n"
		"  -v --verbose  verbose output\n"
		"  -t --terse    shorter output\n"
		"     --csv      output valid csv\n"
		"\n"
		"     --first    halt at first number which exceeds the iteration limit\n"
		"     --last     print the last iteration to stderr\n"
		"  -V --version  print type and limit information\n";
	return name;
}

std::string size_info( void ){
	std::stringstream info;
	info	<< "digit_type: " << sizeof(digit)*8 << " bit "
			<< (std::numeric_limits<digit>::is_signed?"":"un") << "signed "
			<< (std::numeric_limits<digit>::is_integer?"int":"float")
			<< "\nmaximum base:  " << +std::numeric_limits<digit>::max()
			<< std::endl;
	return info.str();
}

int function( std::string arg, digit const base, uint32_t const limit, short verbose, bool first, bool last );

int main( int argc, char **argv ){

	short verbose = 0;
	uint32_t limit = 1000;
	digit base = 10;
	bool first = false;
	bool last = false;
	std::string input;

	if( argc < 2 ){
		std::cout << usage( argv[0] );
		return 1;
	}

	int argi = 0;
	int x = 0; // escape
	unsigned long nbase = base;
	while( argi != -1 ){ // getopt loop
		if( ++x > 1000 ) break; // no infinite loop
		argi = getopt_long( argc, argv, "hb:l:i:vtLV", longopts, nullptr );
		try {
			switch( argi ){
				case 'h':	std::cout << usage( argv[0] );				break;
				case 'b':	nbase = std::stoul( optarg, nullptr, 0 );	break;
				case 'l':	limit = std::stoul( optarg, nullptr, 0 );	break;
				case 'i':	input = optarg;								break;
				case 'v':	++verbose;									break;
				case 't':	--verbose;									break;
				case 'c':	verbose=-2;									break;
				case 'V':	std::cout << size_info();					break;
				case 'f':	first = true;								break;
				case 'L':	last = true;								break;
				case -1 : /* end */		case '?': /* missing arg */		break;
				default: std::cout
						<<   "argi   : " << static_cast<char>(  argi  )
						<< "\noptopt : " << static_cast<char>( optopt )
						<< "\noptarg : " << optarg << std::endl;
			}
		} catch( std::out_of_range const& exc ){
			std::cerr << "out_of_range: " << optarg << "\naccepted range: 2 - "
				<< +std::numeric_limits< digit >::max() << std::endl;
			return 2;
		} catch( std::invalid_argument const& exc ){
			std::cerr << "invalid_argument: \"" << optarg << "\" is not parsable as a number (base 8, 10 or 16)." << std::endl;
			return 2;
		}
	}

	constexpr uint64_t base_max = std::numeric_limits<digit>::max();
	if( nbase > base_max ){
		std::clog
			<< "base " << +nbase
			<< " too large for digit type\n"
			<<  size_info()
			<< "base reduced to " << +base_max
			<< std::endl;
		base = base_max;
	} else {
		base = nbase;
	}

	if( verbose < -1 ){ // csv header
		std::cout
			<< "number,"
			<< "iterations,"
			<< "result"
			<< std::endl;
	}

	for( argi = optind; argi < argc; ++argi ){
		int ret = function( argv[argi], base, limit, verbose, first, last );
		// parse fail
		if( ret == 2 ) return 2;
		// found potential lychrel number - stop xargs
		if( ret == 1 && first ) return 255;
	}

	if( !input.empty() ){
		std::ifstream file{ input };
		std::string word;
		while( file >> word ){
			int ret = function( word, base, limit, verbose, first, last );
			// parse fail
			if( ret == 2 ) return 2;
			// found potential lychrel number - stop xargs
			if( ret == 1 && first ) return 255;
		}
	}

	if( first )	return 1;
	else		return 0;
}

int function( std::string arg, digit const base, uint32_t const limit, short verbose = 0, bool first = false, bool last = false ){
	std::vector<digit> num;
	try{
		num = read_number< std::vector<digit> >( arg, base );
	} catch( std::out_of_range const& exc ){
		std::cerr << "out_of_range: " << exc.what() << std::endl;
		return 2;
	} catch( std::invalid_argument const& exc ){
		std::cerr << "invalid_argument: " << exc.what() << std::endl;
		return 2;
	}
	arg = display( num, base );

	int i = 0;
	bool success = false;
	for( ; i < limit; ++i ){
		auto Fit = num.begin();
		auto Rit = num.rbegin();
		auto Fend = num.end();
		for( ; (*Fit == *Rit) && (Fit != Fend); ++Fit, ++Rit );
		if( Fit == num.end() ){
			success = true;
			if( verbose >= 0 ){
				std::cout << arg << " in "
					<< i << " iteration" << ((i>1)?"s":"")
					<< ", became " << display( num, base ) << std::endl;
			} else {
				std::cout << arg << "," << i << "," << display( num, base ) << std::endl;
			}
			if( last ) std::clog << display( num, base ) << std::endl;
			break;
		}
		if( verbose > 1 ){
			std::clog << display( num, base ) << std::endl;
		} else if( verbose > 0 && ( i > 0 ) && ( i % 1000 == 0 )){
			std::clog << i << ":" << num.size() << std::endl;
		}

		lychrel( num, base );
	}
	if( !success ){
		if( verbose >= 0 ){
			std::cout << arg << " after "
				<< i << " iterations, has FAILED "
				<< "at length " << num.size() << std::endl;
		} else {
			std::cout << arg << "," << i << ",FAILED" << std::endl;
		}
		if( last ) std::clog << display( num, base ) << std::endl;
		return 1;
	}
	return 0;
}
