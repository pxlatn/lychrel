#include <string>
#include <sstream>
#include <iostream>
#include <stdexcept>

const std::string digits("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+/"); // base64

template< typename T >
void lychrel( T& input, typename T::value_type const base ){
	// Allocate size + 1 space to account for potential carries
	T ret( input.size() + 1 );
	auto Fit = input.begin();
	auto Rit = input.rbegin();
	auto Fend = input.end();
	auto out = ret.begin();
	bool carry = false;

	for( ; Fit != Fend; ++Fit, ++Rit, ++out ){
		*out = ( *Fit + *Rit + (carry?1:0) ) % base;
		carry = ( *Fit + *Rit + (carry?1:0) ) >= base;
	}

	// shrink to fit
	if( carry ){
		*out = 1;
	} else {
		ret.erase( out );
	}

	input.swap( ret );
}

/**
 *	None of these functions need to be particularly efficient
 *	They are run once per input number
 *	With an additional run of display() per 1000 iterations if the verbose flag is set
**/

/// ASCII case shift - subtracts 0x20 from any char in the lower-case set
constexpr char uppercase( char input ) {
	return ( input > 0x60 && input < 0x7b)? input - 0x20 : input;
}

/** read input as a number
 *
 *	Depends on base and format
 *		if the input contains commas
 *			we assume it is comma separated digit values and read out the /[0-9]+/ numbers contained within
 *			values too high for base?
 *				throw out_of_range
 *		if the base is lower than the max representable by the digits array
 *			we convert the input to the digit values
 *				values too high?
 *					throw out_of_range
 *		otherwise?
 *			throw invalid_argument
**/
template< typename T >
T read_number( std::string input, typename T::value_type const base ){
	T ret;
	if( input.find_first_of(',') != std::string::npos || base > digits.size() ){
		// interpret input as a comma separated list of digit values
		auto iter = input.begin(),
			end = input.end(),
			A = end;
		for( ; iter != end; ++iter ){
			if( (*iter >= '0') && (*iter <= '9') ){
				if( A == end ) A = iter;
			} else {
				if( A != end ){
					typename T::value_type const d = std::stoul( std::string{ A, iter }, nullptr, 10 );
					if( d < base ){
						ret.insert( ret.begin(), d );
					} else {
						throw std::out_of_range(
							"invalid digit: \"" + std::string{ A, iter } + "\""
							" is not a number between 0 and " + std::to_string( base-1 )
						);
					}
					A = end;
				}
			}
		}
		if( A != end ){
			typename T::value_type const d = std::stoul( std::string{ A, iter }, nullptr, 10 );
			if( d < base ){
				ret.insert( ret.begin(), d );
			} else {
				throw std::out_of_range(
					"invalid digit: \"" + std::string{ A, iter } + "\""
					" is not a number between 0 and " + std::to_string( base-1 )
				);
			}
		}
	} else if( base <= digits.size() ){
		// interpret input as a number
		ret.resize( input.size() ); // allocate the needed number of digits
		auto iit = input.rbegin();
		auto end = input.rend();
		auto oit = ret.begin();
		for( ; iit != end; ++iit, ++oit ){
			char c = *iit;
			if( base <= 36 ) c = uppercase( c );
			size_t value = digits.find_first_of( c );
			if( (value != std::string::npos) && (value < base) ){
				*oit = value;
			} else if( value == std::string::npos){
				throw std::invalid_argument(
					"\"" + std::string{ c } + "\" is not a base64 character."
				);
			} else {
				throw std::out_of_range(
					"\"" + std::string{ c } + "\" is out of the base " + std::to_string( base ) + " range "
					"[ " + digits[0] + " -> " + digits[ base-1 ] + " ]"
				);
			}
		}
	} else {
		throw std::invalid_argument(
			"Cannot convert \"" + input + "\" as a base " + std::to_string( base ) + " number."
		);
	}
	return ret;
}

template< typename T >
std::string display( T input, typename T::value_type const base ){
	std::ostringstream out;
	if( base <= digits.size() ){
		// print characters from the digits array
		for( auto it=input.rbegin(), end=input.rend(); it != end; ++it )
			out << digits[ *it ];
	} else {
		// print a quoted, comma-separated, list of the digits (big-endian)
		out << "\"";
		bool first = true;
		for( auto it=input.rbegin(), end=input.rend(); it != end; ++it ){
			if( !first ){
				out << ",";
			} else {
				first = false;
			}
			// output *it as a number
			out << +*it;
		}
		out << "\"";
	}
	return out.str();
}
