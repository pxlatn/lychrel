#include <string>
#include <list>
#include <deque>
#include <vector>
#include <iostream>

#include <chrono>
#include <getopt.h>

#include "methods.h"

// digit type
using digit = uint8_t;

std::string addA( std::string A, std::string B, int const base );
std::string addB( std::string input, int const base );
std::list<digit> addC( std::list<digit> input, int const base );
std::deque<digit> addD( std::deque<digit> input, int const base );
std::string addE( std::string input, int const base );

// Options - global because they may be queried globally
const struct option longopts[] = {
	{ "help",		no_argument,	0, 'h' },
	{ "verbose",	no_argument,	0, 'v' },
	{ "limit",	required_argument,	0, 'l' },
	{ "base",	required_argument,	0, 'b' },
	{ 0, 0, 0, 0 }
};

std::string usage( std::string name ){
	name += "\n"
		"  -h  this help\n"
		"  -v  verbose output\n"
		"  -l  iteration limit (default: 1000)\n"
		"  -b  numeric base (2-64) (default: 10)\n";
	return name;
}

int main( int argc, char **argv ){
	short verbose = false;
	int limit = 1000;
	int base = 10;

	int argi = 0;
	int x = 0; // escape
	while( argi != -1 ){ // getopt loop
		if( ++x > 1000 ) break; // no infinite loop
		argi = getopt_long( argc, argv, "hvl:b:", longopts, nullptr );
		try {
			switch( argi ){
				case 'v':	++verbose;						break;
				case 'l':	limit =	std::stoul( optarg, nullptr, 0 );	break;
				case 'b':	base =	std::stoul( optarg, nullptr, 0 );	break;
				case -1:									break;
				case 'h':	std::cout << usage( argv[0] );	break;
				default: std::cout
						<<   "argi   : " << static_cast<char>(  argi  )
						<< "\noptopt : " << static_cast<char>( optopt )
						<< "\noptarg : " << optarg << std::endl;
			}
		} catch( std::out_of_range const& exc ){
			std::clog << "out_of_range: " << optarg << "\naccepted range: 2 - "
				<< +std::numeric_limits< digit >::max() << std::endl;
			return 2;
		} catch( std::invalid_argument const& exc ){
			std::clog << "invalid_argument: \"" << optarg << "\" is not parsable as a number (base 8, 10 or 16)." << std::endl;
			return 2;
		}
	}

	constexpr auto base_max = std::numeric_limits<digit>::max();
	if( base > base_max ) base = base_max;
	if( verbose > 1 )
		std::clog << "base: " << base << "\nmax:  " << +base_max << std::endl;

	int i{0};
	int step = 1000;

	// headers
	std::cout << "method,iterations,time,characters" << std::endl;

	{ // A - two strings as input, store digit values
		std::string in{ "196" };
		std::string inr;

		auto start = std::chrono::high_resolution_clock::now();
			for( i=0; i<limit; ++i ){
				inr = { in.rbegin(), in.rend() };
				in = addA( in, inr, base );
				if( verbose && i % step == 0 ) std::cerr << "A";
			}
			if( verbose ) std::cerr << std::endl;
		auto end = std::chrono::high_resolution_clock::now();

		std::chrono::duration<double> time = end - start;
		std::cout << "A," << limit << "," << time.count() << "," << in.size() << std::endl;
	}
	{ // B - one string as input, base 10 only, store digit values
		std::string in{ "196" };

		auto start = std::chrono::high_resolution_clock::now();
			for( i=0; i<limit; ++i ){
				in = addB( in, base );
				if( verbose && i % step == 0 ) std::cerr << "B";
			}
			if( verbose ) std::cerr << std::endl;
		auto end = std::chrono::high_resolution_clock::now();

		std::chrono::duration<double> time = end - start;
		std::cout << "B," << limit << "," << time.count() << "," << in.size() << std::endl;
	}
	{ // C - list input,  store digits
		std::list<digit> in{ 1, 9, 6 };

		auto start = std::chrono::high_resolution_clock::now();
			for( i=0; i<limit; ++i ){
				in = addC( in, base );
				if( verbose && i % step == 0 ) std::cerr << "C";
			}
			if( verbose ) std::cerr << std::endl;
		auto end = std::chrono::high_resolution_clock::now();

		std::chrono::duration<double> time = end - start;
		std::cout << "C," << limit << "," << time.count() << "," << in.size() << std::endl;
	}
	{ // D - deque input, store digits
		std::deque<digit> in{ 1, 9, 6 };

		auto start = std::chrono::high_resolution_clock::now();
			for( i=0; i<limit; ++i ){
				in = addD( in, base );
				if( verbose && i % step == 0 ) std::cerr << "D";
			}
			if( verbose ) std::cerr << std::endl;
		auto end = std::chrono::high_resolution_clock::now();

		std::chrono::duration<double> time = end - start;
		std::cout << "D," << limit << "," << time.count() << "," << in.size() << std::endl;
	}
	{ // E - B with string::reserve()
		std::string in{ "196" };

		auto start = std::chrono::high_resolution_clock::now();
			for( i=0; i<limit; ++i ){
				in = addE( in, base );
				if( verbose && i % step == 0 ) std::cerr << "E";
			}
			if( verbose ) std::cerr << std::endl;
		auto end = std::chrono::high_resolution_clock::now();

		std::chrono::duration<double> time = end - start;
		std::cout << "E," << limit << "," << time.count() << "," << in.size() << std::endl;
	}
	{ // F - lychrel() w/ std::list
		auto in = read_number< std::list<digit> >( "196", base );

		auto start = std::chrono::high_resolution_clock::now();
			for( i=0; i<limit; ++i ){
				lychrel( in, base );
				if( verbose && i % step == 0 ) std::cerr << "F";
			}
			if( verbose ) std::cerr << std::endl;
		auto end = std::chrono::high_resolution_clock::now();

		std::chrono::duration<double> time = end - start;
		std::cout << "F," << limit << "," << time.count() << "," << in.size() << std::endl;
	}
	{ // G - lychrel() w/ std::deque
		auto in = read_number< std::deque<digit> >( "196", base );

		auto start = std::chrono::high_resolution_clock::now();
			for( i=0; i<limit; ++i ){
				lychrel( in, base );
				if( verbose && i % step == 0 ) std::cerr << "G";
			}
			if( verbose ) std::cerr << std::endl;
		auto end = std::chrono::high_resolution_clock::now();

		std::chrono::duration<double> time = end - start;
		std::cout << "G," << limit << "," << time.count() << "," << in.size() << std::endl;
	}
	{ // H - lychrel() w/ std::vector
		auto in = read_number< std::vector<digit> >( "196", base );

		auto start = std::chrono::high_resolution_clock::now();
			for( i=0; i<limit; ++i ){
				lychrel( in, base );
				if( verbose && i % step == 0 ) std::cerr << "H";
			}
			if( verbose ) std::cerr << std::endl;
		auto end = std::chrono::high_resolution_clock::now();

		std::chrono::duration<double> time = end - start;
		std::cout << "H," << limit << "," << time.count() << "," << in.size() << std::endl;
	}

	return 0;
}

// A - two strings as input, store digit values
std::string addA( std::string A, std::string B, int const base ){
	std::string ret;
	auto Ait = A.rbegin();
	auto Bit = B.rbegin();
	uint8_t carry = 0;
	for(;;){
		uint8_t Aval = (Ait < A.rend())? digits.find_first_of( *Ait ) : 0;
		uint8_t Bval = (Bit < B.rend())? digits.find_first_of( *Bit ) : 0;
		uint8_t calc = Aval + Bval + carry;
		carry = 0;
		if( calc >= base ){
			auto t = calc;
			calc %= base;
			carry = (t - calc)/base;
		}
		ret.insert( 0,1,digits[ calc ] );
		if( Ait < A.rend() ) ++Ait;
		if( Bit < B.rend() ) ++Bit;
		if( Ait == A.rend() && Bit == B.rend() && carry == 0) break;
	}
	return ret;
}

// B - one string as input, base 10 only, store digit values
std::string addB( std::string input, int const base ){
//	if( base != 10 ){
//		return addA( input, std::string( input.rbegin(), input.rend() ));
//	}
	std::string ret;
	auto Ait = input.begin();
	auto Bit = input.rbegin();
	auto end = input.end();
	uint8_t carry = 0;
	do{
		uint8_t Aval = *Ait - 0x30;
		uint8_t Bval = *Bit - 0x30;
		uint8_t calc = Aval + Bval + carry;
		carry = 0;
		if( calc >= base ){
			auto t = calc;
			calc %= base;
			carry = (t - calc)/base;
		}
		ret.insert( 0,1,digits[ calc ] );
		++Ait; ++Bit;
	} while( Ait != end );
	if( carry ) ret.insert( 0,1,digits[1] );
	return ret;
}

// C - list input,  store digits
std::list<digit> addC( std::list<digit> input, int const base ){
	std::list<digit> ret;
	auto Fit = input.begin();
	auto Rit = input.rbegin();
	auto Fend = input.end();
	bool carry = false;
	do{
		ret.emplace_front( (*Fit + *Rit + (carry?1:0)) % base );
		carry = (*Fit + *Rit + (carry?1:0)) >= base;
		++Fit; ++Rit;
	} while( Fit != Fend );
	if( carry ) ret.emplace_front( 1 );
	return ret;
}

// D - deque input, store digits
std::deque<digit> addD( std::deque<digit> input, int const base ){
	std::deque<digit> ret;
	auto Fit = input.begin();
	auto Rit = input.rbegin();
	auto Fend = input.end();
	bool carry = false;
	do{
		ret.emplace( ret.begin(), (*Fit + *Rit + (carry?1:0)) % base );
		carry = (*Fit + *Rit + (carry?1:0)) >= base;
		++Fit; ++Rit;
	} while( Fit != Fend );
	if( carry ) ret.emplace( ret.begin(), 1 );
	return ret;
}

// E - B with string::reserve()
std::string addE( std::string input, int const base ){
//	if( base != 10 ){
//		return addA( input, std::string( input.rbegin(), input.rend() ));
//	}
	std::string ret;
	ret.reserve( input.size()+1 );
	auto Ait = input.begin();
	auto Bit = input.rbegin();
	auto end = input.end();
	uint8_t carry = 0;
	do{
		uint8_t Aval = *Ait - 0x30;
		uint8_t Bval = *Bit - 0x30;
		uint8_t calc = Aval + Bval + carry;
		carry = 0;
		if( calc >= base ){
			auto t = calc;
			calc %= base;
			carry = (t - calc)/base;
		}
		ret.insert( 0,1,digits[ calc ] );
		++Ait; ++Bit;
	} while( Ait != end );
	if( carry ) ret.insert( 0,1,digits[1] );
	return ret;
}
